/* 
 * File:   lcd.h
 * Author: MCF009
 *This is a free generic driver. You can distribute within yourself, modify it and redistribute.
 * Created on December 19, 2014, 11:18 AM
 */

#ifndef LCD_H
#define	LCD_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "lcdconf.h"
#include "lcddef.h"


#if LCD_HD44780

//typedef BYTE STATUS;    //LCD status feedback
#define LCD_RD 0
#define LCD_WR 1

#define CLEAR_ALL 0
#define CLEAR_LINE1 1
#if LCD_2LINES
#define CLEAR_LINE2 2
#endif
#if LCD_4LINES 
#define CLEAR_LINE3 3
#define CLEAR_LINE4 4
#endif

#define LINE_1 0x80
#if LCD_2LINES 
#define LINE_2 0xc0
#endif
#if LCD_4LINES 
#if LENGTH_16 
    #define LINE_3 0x90
    #define LINE_4 0xd0
#endif
#if LENGTH_20
    #define LINE_3 0x94
    #define LINE_4 0xd4
#endif
#endif

//commands
#define B_ON 1 //lcd backlight on
#define B_OFF 0 //lcd backlight off
#define CLR_DISPLAY 0x01 //clear lcd display
#define RETURN_HOME 0x02    //return home position

#define ENTRY_CONTROL 0x04 //entry mode register select
#define INCREMENT 0x02 //increment by one
#define DECREMENT 0X00 //decrement by one
#define SHIFT_EN 0x01 //shift enabled

#define DISPLAY_CONTROL 0x08 //display control register select
#define DISPLAY_ON 0x04 //display on
#define CURSOR_ON 0x02 //cursor on
#define BLINK_ON 0x01 //blinking on

#define CURSOR_CONTROL 0x10 //cursor control register select
#define MOVE_DISPLAY 0X08 //move display
#define MOVE_CURSOR 0x00 //move cursor
#define SHIFT_RIGHT 0x04 //shift to right
#define SHIFT_LEFT 0x00 //shift to left

#define FUNCTION_CONTROL 0x20 //function control register select
#define INTERFACE8_BIT 0X10 //8 bit data interface select
#define INTERFACE4_BIT 0X00 //4 bit data interface select
#define LINE1 0x00 //one line select
#define LINE2 0x08 //two lines select
#define DOTS5_10 0x04 //5x10 dots character select (only 1 line possible in this mode)
#define DOTS5_8 0x00 //5x8 dots character select

#define LCD_BUSY 0x80 //in read mode busy flag on



#if INTERFACE_8_BIT  //8bit mode
//define your ports here for 8bit mode
//#define LCD_PORT
#else   //4bit mode
//define your ports here for 4bit mode
//#define LCD_PORT
#endif
//#define LCD_RW
//#define LCD_RS
//#define LCD_EN
//#define BCK_LGHT 



//IO LEVEL FUNCTIONS//

void lcd_data(void);    //select lcd data mode
void lcd_cmd(void); //select lcd command mode
void lcd_en(void);  //LCD latch enable
void lcd_rw(BYTE sel); //lcd read write select
void lcd_write(BYTE databuffer); //lcd write data
void lcd_set_line(BYTE line); //set cursor (0 to 15) positions to line (1/2)
void lcd_printf(BYTE line);//print the buffers
void lcd_clearf(BYTE line);//clear the buffers

//APPLICATION PERIPHERAL INTERFACE (you need to use only these functions as API level layer)
void api_lcd_initialize(void); //lcd initialize
void api_lcd_printf(BYTE line, BYTE cursor, CHAR8 *databuffer, BYTE data_length);
void api_lcd_clearf(BYTE clear_sel);
void api_lcd_custom_character_ram(char *cgram);
#if BACKLIGHT_CONTROL
void api_lcd_backlight(BYTE state);
#endif
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* LCD_H */

