/* 
 * File:   main.c
 * Author: MCF009
 *
 * Created on December 30, 2014, 8:31 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <pic16f877a.h>
#include "lcd.h"

// CONFIG
#pragma config FOSC = XT        // Oscillator Selection bits (XT oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)
#pragma config LVP = ON         // Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit (RB3/PGM pin has PGM function; low-voltage programming enabled)
#pragma config CPD = OFF        // Data EEPROM Memory Code Protection bit (Data EEPROM code protection off)
#pragma config WRT = OFF        // Flash Program Memory Write Enable bits (Write protection off; all program memory may be written to by EECON control)
#pragma config CP = OFF         // Flash Program Memory Code Protection bit (Code protection off)

int main(void)
{
    char test[]="This is a test";
    api_lcd_initialize(); //initialize 16x2 lcd 8bit data bus backlight controlled
    api_lcd_backlight(B_ON); //backlight on
    api_lcd_printf(1, 1, test, 14);
    while(1);
    return 0;
}

void delay_us(unsigned int n) //delay routine
{
    unsigned int i;
    while(n--)
    {
        i=100;
        while(i--);
    }
}