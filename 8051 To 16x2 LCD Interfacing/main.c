///////////////////////////////////////////////////////////
// Company         : Micro Digital                       //
// Address         : Office # C7 Raza Plaza              //
//                   DAV College Road                    //
//                   Rawalpindi,                         //
//                   Pakistan.                           //
// Programmed By   : Rashid Mehmood                      //
// Project Name    : 8051 To 16x2 LCD Interfacing        //
// Crystal         : 24.000000 MHz                       //
// Microcontroller : AT89C51-C52-C55-S51-S52             //
///////////////////////////////////////////////////////////

#include <AT89X51.H>
#include "lcd4bit.h"

void main()
{
	unsigned char i = 0;
	init_lcd();
	print_lcd("  Micro Digital  \0");
	gotoxy_lcd(1, 2);
	print_lcd("micro-digital.net\0");
	while(1)
	{
	}
}

