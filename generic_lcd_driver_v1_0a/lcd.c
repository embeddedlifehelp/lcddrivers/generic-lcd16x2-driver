/*
 * File:   lcddef.h
 * Author: MCF009
 *
 * Created on December 19, 2014, 11:28 AM
 * This is a free generic driver. You can distribute within yourself, modify it and redistribute.
 */
#include "lcd.h"

#if LCD_HD44780


    BYTE lcd_buffer_1[LCD_LENGTH];
#if LCD_2LINES 
    BYTE lcd_buffer_2[LCD_LENGTH];
#endif
#if LCD_4LINES
        BYTE lcd_buffer_3[LCD_LENGTH];
        BYTE lcd_buffer_4[LCD_LENGTH];
#endif

void api_lcd_initialize(void) //lcd initialize
{
    //enter your initialization code here
    //150ms delay here
     lcd_cmd();
     lcd_rw(LCD_WR);
#ifdef INTERFACE_8_BIT
     lcd_write(FUNCTION_CONTROL|INTERFACE8_BIT|LINE2|DOTS5_8);     //This instruction will Enable 8-bit Databus, Set 2 lines, and Select font size 5x8
     #else
     lcd_write(FUNCTION_CONTROL|INTERFACE4_BIT|LINE2|DOTS5_8);     //This instruction will Enable 4-bit Databus, Set 2 lines, and Select font size 5x8
#endif
     lcd_en();
     lcd_en();
     lcd_en();
     lcd_en();
     lcd_write(DISPLAY_CONTROL|DISPLAY_ON);       //It will display the characters, will not display the cursor
     lcd_en();
     lcd_write(LINE_1);    //Set cursor on line 1
     lcd_en();
     lcd_write(CLR_DISPLAY);       //Clear screen
     lcd_en();
     lcd_data();
}

#ifdef BACKLIGHT_CONTROL
void api_lcd_backlight(BYTE state)
{
    switch(state)
    {
        case B_OFF: //enter your code here

        case B_ON:    //enter your code here

        default:break;
    }
}
#endif

void lcd_data(void)    //select lcd data mode
{
    //enter your code here
}
void lcd_cmd(void) //select lcd command mode
{
    //enter your code here
}
void lcd_en(void)  //LCD latch enable
{
    //enter your code here
}
void lcd_rw(BYTE sel) //lcd read write select
{
    switch(sel)
    {
        case LCD_RD: //enter your code here

        case LCD_WR:    //enter your code here

        default:break;
    }
}

void lcd_write(BYTE databuffer) //lcd write data
{
    #if INTERFACE_8_BIT  //8bit mode

    //enter your code here

   #else   //4bit mode

    //enter your code here

    #endif
}



void lcd_set_line(BYTE line) //set cursor (0 to 15) positions to line (1/2)
{
    lcd_cmd();  
    lcd_rw(LCD_WR);
    switch(line)
    {
        case 1:lcd_write(LINE_1);break;
#if LCD_2LINES 
        case 2:lcd_write(LINE_2);break;
#endif
#if LCD_4LINES
        case 3:lcd_write(LINE_3);break;
        case 4:lcd_write(LINE_4);break;
#endif
        default:break;
    }
    lcd_en();
    lcd_data();

}

void lcd_printf(void)//print the buffers
{
    BYTE i=0;
   lcd_set_line(1);
   for(i=0;i<=(LCD_LENGTH-1);i++){lcd_write(lcd_buffer_1[i]);lcd_en();}
    #if LCD_2LINES
        lcd_set_line(2);
        for(i=0;i<=(LCD_LENGTH-1);i++){lcd_write(lcd_buffer_2[i]);lcd_en();}
    #endif
    #if LCD_4LINES
        lcd_set_line(3);
        for(i=0;i<=(LCD_LENGTH-1);i++){lcd_write(lcd_buffer_3[i]);lcd_en();}
        lcd_set_line(4);
        for(i=0;i<=(LCD_LENGTH-1);i++){lcd_write(lcd_buffer_4[i]);lcd_en();}
    #endif
}

void lcd_clearf(void)//clear the buffers
{
    BYTE i=0;
    for(i=0;i<=(LCD_LENGTH-1);i++)
    {
        lcd_buffer_1[i]=' ';
        #if LCD_2LINES 
            lcd_buffer_2[i]=' ';
        #endif
        #if LCD_4LINES 
            lcd_buffer_3[i]=' ';
            lcd_buffer_4[i]=' ';
        #endif
    }
    lcd_printf();
}

#endif

void api_lcd_printf(BYTE line, BYTE cursor, CHAR8 *databuffer, BYTE data_length)
{
    BYTE i=0,j=0;
    i=cursor-1;
    switch(line)
    {
        case 1:
        {
            while(j<=(data_length-1))
            {
                lcd_buffer_1[i]=databuffer[j];
                j++;
                i++;
            }
        }break;
        #if LCD_2LINES 
            case 2:
            {
                while(j<=(data_length-1))
                {
                    lcd_buffer_2[i]=databuffer[j];
                    j++;
                    i++;
                }
            }break;
         #endif
         #if LCD_4LINES 
            case 3:
            {
                while(j<=(data_length-1))
                {
                    lcd_buffer_3[i]=databuffer[j];
                    j++;
                    i++;
                }
            }break;
            case 4:
            {
                while(j<=(data_length-1))
                {
                    lcd_buffer_4[i]=databuffer[j];
                    j++;
                    i++;
                }
            }break;
         #endif
        default:break;
    }
    lcd_printf();
}


void api_lcd_clearf(BYTE clear_sel)
{
    BYTE i=0;
    switch(clear_sel)
    {
        case CLEAR_ALL: lcd_clearf();break;
        case CLEAR_LINE1: for(i=0;i<=(LCD_LENGTH-1);i++)lcd_buffer_1[i]=' ';lcd_printf();break;
        #if LCD_2LINES 
            case CLEAR_LINE2: for(i=0;i<=(LCD_LENGTH-1);i++)lcd_buffer_2[i]=' ';lcd_printf();break;
        #endif
        #if LCD_4LINES
            case CLEAR_LINE3: for(i=0;i<=(LCD_LENGTH-1);i++)lcd_buffer_3[i]=' ';lcd_printf();break;
            case CLEAR_LINE4: for(i=0;i<=(LCD_LENGTH-1);i++)lcd_buffer_4[i]=' ';lcd_printf();break;
        #endif
        default:break;
    }   
}

void api_lcd_custom_character_ram(char *cgram)
  {

          BYTE k=0;
          lcd_cmd();
          lcd_write(0x40); //Send the instruction to set CGRAM address to store your custom character.
          lcd_en(); //Latch this data once.
          lcd_data();//Enable data mode.
          //Next you have to send 8bytes of character data. Do like this below.

          for(k=0;k<=63;k++)
          {
                lcd_write(cgram[k]);
                lcd_en();
          }
          //After 8 bytes transfer you must run a clear screen once, I am having issues in reading data here, so i used this instruction to overcome.
          lcd_cmd();
          lcd_write(CLR_DISPLAY);//clear screen
          lcd_en();
          lcd_data();
  }


