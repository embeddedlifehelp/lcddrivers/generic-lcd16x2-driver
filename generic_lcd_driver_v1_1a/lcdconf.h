/* 
 * File:   lcdcon.h
 * Author: MCF009
 *This is a free generic driver. You can distribute within yourself, modify it and redistribute.
 * Created on December 19, 2014, 11:16 AM
 */

#ifndef LCDCONF_H
#define	LCDCONF_H

#ifdef	__cplusplus
extern "C" {
#endif

#define LCD_LENGTH 16 //max characters [16] [20] are supported
#define LENGTH_16 1 //16x2 / 16x4 lcd supported LENGTH_20 needs to be set to 0
#define LENGTH_20 0 //20x2 / 20x4 lcd supported LENGTH_16 needs to be set to 0
#define LCD_HD44780 1 //HD44780 controller enabled
#define LCD_2LINES 1 //LCD 16x2 enabled else 16x1
#define LCD_4LINES 0 //LCD 16x4 enabled else 16x2 but you need to turn LCD_16_2 =1 to use this feature
#define INTERFACE_8_BIT 1 //1 = 8bit data interface enabled / 0 = 4bit data interface
#define BACKLIGHT_CONTROL 0 //1 = io controlled backlight control / 0 = backlight wired through hardware




#ifdef	__cplusplus
}
#endif

#endif	/* LCDCONF_H */

