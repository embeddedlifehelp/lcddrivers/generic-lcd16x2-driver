/* 
 * File:   lcddef.h
 * Author: MCF009
 *This is a free generic driver. You can distribute within yourself, modify it and redistribute.
 * Created on December 19, 2014, 11:28 AM
 */

#ifndef LCDDEF_H
#define	LCDDEF_H

#ifdef	__cplusplus
extern "C" {
#endif

typedef unsigned char BYTE;
typedef char CHAR8;




#ifdef	__cplusplus
}
#endif

#endif	/* LCDDEF_H */

