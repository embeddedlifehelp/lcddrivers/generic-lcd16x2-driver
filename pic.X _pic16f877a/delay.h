/* 
 * File:   delay.h
 * Author: MCF009
 *
 * Created on December 30, 2014, 8:50 PM
 */

#ifndef DELAY_H
#define	DELAY_H

#ifdef	__cplusplus
extern "C" {
#endif

    void delay_us(unsigned int n); //delay routine



#ifdef	__cplusplus
}
#endif

#endif	/* DELAY_H */

